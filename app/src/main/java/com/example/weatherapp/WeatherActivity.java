package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class WeatherActivity extends AppCompatActivity {
    TextView city, temperature, humidity, pressure;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        city = findViewById(R.id.city);
        temperature = findViewById(R.id.temperature);
        humidity = findViewById(R.id.humidity);
        pressure = findViewById(R.id.pressure);

        city.setText(getIntent().getStringExtra("City"));
        temperature.setText(getIntent().getStringExtra("Temp"));
        humidity.setText(getIntent().getStringExtra("Hum"));
        pressure.setText(getIntent().getStringExtra("Pressure"));


    }
}