package com.example.weatherapp;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    Button SearchBtn;
    EditText CityName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CityName = findViewById(R.id.city);
        SearchBtn =findViewById(R.id.search_bar);
        SearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String result = CityName.getText().toString();

                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();

                if(!"".equals(result)){

                    RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                    String url = "https://api.openweathermap.org/data/2.5/weather?q="+result+"&appid=2d1b74f5ad0d95acfc6cf9bf6bdd5687&units=metric";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                            (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    // textView.setText("Response: " + response.toString());
                                    String City = "";
                                    String Pressure = "Pressure : ";
                                    String Humidity = "Humidity : ";
                                    String Temp = "Temp : ";

                                    try {
                                        City = response.getString("name");
                                        Pressure += response.getJSONObject("main").getString("pressure") + " psi";
                                        Temp += response.getJSONObject("main").getString("temp") + " degree C";
                                        Humidity += response.getJSONObject("main").getString("humidity") + "%";
                                        Intent intent = new Intent(MainActivity.this, WeatherActivity.class);
                                        intent.putExtra("City",City);
                                        intent.putExtra("Pressure",Pressure);
                                        intent.putExtra("Temp",Temp);
                                        intent.putExtra("Hum",Humidity);
                                        startActivity(intent);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    new AlertDialog.Builder(MainActivity.this)
                                            .setTitle("Alert")
                                            .setMessage("No City Found With This Name")
                                            .setCancelable(true)
                                            .show();
                                }
                            });
                    queue.add(jsonObjectRequest);
                }else
                {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Filed Required")
                            .setMessage("Please Enter City Name")
                            .setCancelable(true)
                            .show();
                }

            }
        });
    }
}